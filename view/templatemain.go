package main

import (
	"template"
	"os"
)

type Tradesman struct {
	Id int
	Name string
	Phone string
	Email string
	Description string
}

func main(){

	t := template.New(nil)
	if err := t.ParseFile("templates/maintenance.tpl.html"); err != nil {
		panic(err)
	}
	if err := t.Execute(os.Stdout,struct{Tradesmen []Tradesman}{[]Tradesman{Tradesman{1,"guy","","",""}}}); err != nil {
		panic(err)
	}
}
