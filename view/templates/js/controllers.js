/* App Controllers */


function MyCtrl1() {}
MyCtrl1.$inject = [];


function MyCtrl2() {
}
MyCtrl2.$inject = [];


function PhoneCatCtrl($route) {
  var self = this;

  $route.when('/phones',
      {template: 'partials/editContact.tpl.html'});
  $route.when('/phones/:phoneId',
      {template: 'partials/editTradesman.tpl.html'});
  $route.otherwise({redirectTo: '/phones'});

  $route.onChange(function() {
    self.params = $route.current.params;
  });

  $route.parent(this);
}

function HouseCtrl($xhr) {
        self = this
        $xhr('GET', '/property/0', function(code, response) {
        self.Property = response;
        });

this.ListTradesman = [{id:"1",name:"Pete"},{id:"2",name:"James"},{id:3,name:"Joe"},{id:4,name:"nelix"},{id:5,name:"dude"}];
this.ListRequestType = [{name:"ThirdSchedule",id:1},
        {name:"Cyclical",id: 2},
        {name:"Urgent",id: 3},
        {name:"Ongoing",id: 4},
        {name:"Other",id: 5}]

this.Request = {};
this.Request.Property = this.Property;
this.Request.RequestBy = "me";
this.Request.Tradesman = [this.ListTradesman[1],this.ListTradesman[2]];
this.Request.Description = "kitchen floor needs replacing";
this.Request.RequestType = "";

}

function TenancyCtrl() {
        this.Tenant = {}
        this.Tenant.IsMember = true;
        this.Tenant.Name = "Jesse McNelis";
        this.Tenant.Phone = ["0394199157"];
        this.Tenant.Email = ["jessta@gmail.com"];
        this.Tenant.Description = "home during the day";
}


