package main

import (
	"encoding/json"
	"fmt"
	"github.com/codeforsystemsbiology/rest.go"
	"net/http"
	"strconv"
	"sync"
	"github.com/jessta/huis/model/property"
)

type Properties struct {
	Records []*property.Property
	sync.Mutex
}

func (p *Properties) Index(w http.ResponseWriter) {
	p.Lock()
	defer p.Unlock()
	for i := range p.Records {
		w.Write([]byte(fmt.Sprintf("%d, %v\n", (*p).Records[i].Id, (*p).Records[i].Address)))
	}
}

func (p *Properties) Create(w http.ResponseWriter, r *http.Request) {
	p.Lock()
	defer p.Unlock()
	println("created new property")
	newP := new(property.Property)
	(*p).Records = append((*p).Records, newP)
	rest.Created(w, strconv.Itoa(len((*p).Records)-1))
}

func (p *Properties) Find(w http.ResponseWriter, id string) {
	p.Lock()
	defer p.Unlock()
	nid, err := strconv.Atoi(id)
	if err != nil || len((*p).Records)-1 < nid {
		rest.NotFound(w)
		return
	}
	println(id, ":", len((*p).Records))
	b, err := json.MarshalIndent((*p).Records[nid], "", "")
	if err != nil || len((*p).Records)-1 < nid {
		rest.NotFound(w)
		return
	}
	w.Write(b)
}
