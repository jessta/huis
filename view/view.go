package view

func (q *Quotes) Index(w http.ResponseWriter){
	//fetch list of Quotes
	*q = append(*q,Quote{"replacement of stove",600.0,&Tradesman{}})
	j,err := json.Marshal(q)
	if err != nil {
		http.Error(w, "internal error", 500)
		return
	}
	w.Write(j)
}
