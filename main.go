package main

import (
	"encoding/json"
 "net/http"
 "fmt"
 rest "github.com/codeforsystemsbiology/rest.go"
 "github.com/jessta/huis/model/maintenance"
"github.com/jessta/huis/model/property"
"time"
)
var PropertyMux = http.NewServeMux()

func main() {

	/*ACL*/
	a := new(maintenance.Quote)
	a.Tradesman = new(maintenance.Tradesman)
	a.Tradesman.Name = "guy"
	b, err := json.Marshal(a)
	if err != nil {
		panic(err)
	}
	println(string(b))
	var properties *Properties = new(Properties)
	for i := 0; i < 10; i++ {
		p := new(property.Property)
		p.Id = i
		p.Address = "314 Wellington St, Collingwood 3066"
		(*properties).Records = append((*properties).Records, p)
	}
	(*properties).Records[0].NewCyclicalProvision("Hot Water Service", time.Now(),5000.0)
	(*properties).Records[1].NewCyclicalProvision("Hot Water Service", time.Now(),5000.0)
	(*properties).Records[2].NewCyclicalProvision("Hot Water Service", time.Now(),5000.0)
	fmt.Println((*properties).Records[2])

	rest.Resource("property", properties)

	http.Handle("/", http.FileServer(http.Dir("/home/jessta/go/src/pkg/github.com/jessta/huis/")))
	/*var meetings meeting.Meetings
	rest.Resource("/meeting/", meetings)

	rest.Resource("/member/", member.Members)
	rest.Resource("/maintenance/", )
	rest.Resource("/corespondance/", properties)
	*/
	/*
		Members := MemberController
		mux := http.NewServeMux()
	*/
	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err)
	}
}
