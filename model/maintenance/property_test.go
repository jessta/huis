package maintenance
import "testing"
import "json"

func TestRequest(t *testing.T){
	r := NewRequest(new(Property))
	r.Description = "replacement of cork tiles in kitchen"
	r.Quotes = []Quote{Quote{"will replace and seal cork tiles in kitchen",6000,&Tradesman{600,"","","",""}}}
	
	a, err := json.Marshal(r)
	if err != nil {panic(err)}
	println(string(a))
}
