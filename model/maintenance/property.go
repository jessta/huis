package maintenance
import "github.com/jessta/huis/model/admin"

import (
	"strconv"
	"time"
)

type RequestCategory uint8

const (
	ThirdSchedule RequestCategory = 1
	Cyclical      RequestCategory = 2
	Urgent        RequestCategory = 3
	Ongoing       RequestCategory = 4
	Other         RequestCategory = 5
)

type Tradesman struct {
	admin.Contact
	Company string
	ABN     string
}

/*
	When Marshaling the tradesman in a quote, just include the id
*/
func (t *Tradesman) MarshalJSON() ([]byte, error) {
	if t == nil {
		return []byte("null"), nil
	}
	return []byte(strconv.FormatInt(t.Id, 10)), nil
}

type Quotes []Quote

type Quote struct {
	Description string
	Amount      float64
	Tradesman   *Tradesman
}


type CyclicalProvision struct {
	Item     string
	Amount   float64
	Due time.Time
}

type Request struct {
	Quotes        []Quote
	DateRecieved  time.Time
	DateCompleted time.Time
	DateApproved  time.Time
	Category      RequestCategory
	ApprovedBy    string
	Description   string
}

/*
GET /resource/ => Index(http.ResponseWriter)
GET /resource/id => Find(http.ResponseWriter, id string)
POST /resource/ => Create(http.ResponseWriter, *http.Request)
PUT /resource/id => Update(http.ResponseWriter, id string, *http.Request)
DELETE /resource/id => Delete(http.ResponseWriter, id string)
OPTIONS /resource/ => Options(http.ResponseWriter, id string)
OPTIONS /resource/id => Options(http.ResponseWriter, id string)*/
