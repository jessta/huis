package tenancy

import (
	"github.com/jessta/huis/model/admin"
	"github.com/jessta/huis/model/property"
	"time"
)

type Tenancy struct {
	Tenant admin.Contact
	Property *property.Property
	RentPayments []RentPayment
	RentAssessments []RentAssessment
}

type RentPayment struct {
	Amount float64
	Date time.Time
	Description string
}

type RentAssessment struct {
	WeeklyPayableAmount float64
	StartDate time.Time
	EndDate time.Time
	Description string
}
