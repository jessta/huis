package admin

import "time"
//import "user"


type Member struct {
	//user.User
	Contact
}

type Director struct {
	Member
	Role      string
	StartDate time.Time
	EndDate   time.Time
}

type Corespondance struct {
	To            string
	From          string
	File          string
	DateReceived  time.Time
	DateResponded time.Time
}

type Contact struct {
	Id          int64
	Name        string
	Phone       []string
	Email       []string
	Description string
}

type Meeting struct {
	Minutes   string
	Agenda    string
	Date      int64
	Attended  []Member
	Apologies []Member
}
