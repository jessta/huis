package property
import "github.com/jessta/huis/model/admin"
import "github.com/jessta/huis/model/maintenance"

import (
	"time"
)

type Tenancy struct {
	Id       int64
	Tenant   Tenant
}

type Tenant struct {
	IsMember bool
	admin.Contact
}

type Property struct {
	Id                 int
	Address       string
	CyclicalProvisions []maintenance.CyclicalProvision
	Requests []maintenance.Request
}

func NewProperty() *Property {
	return new(Property)
}

func (p *Property) NewCyclicalProvision(item string, due time.Time, amount float64) *maintenance.CyclicalProvision {
	c := maintenance.CyclicalProvision{item,amount,due}
	p.CyclicalProvisions = append(p.CyclicalProvisions, c)
	return &c
}



func (p *Property) NewRequest() *maintenance.Request {
	var r maintenance.Request
	r.DateRecieved = time.Now()
	p.Requests = append(p.Requests, r)
	return &r
}

/*
GET /resource/ => Index(http.ResponseWriter)
GET /resource/id => Find(http.ResponseWriter, id string)
POST /resource/ => Create(http.ResponseWriter, *http.Request)
PUT /resource/id => Update(http.ResponseWriter, id string, *http.Request)
DELETE /resource/id => Delete(http.ResponseWriter, id string)
OPTIONS /resource/ => Options(http.ResponseWriter, id string)
OPTIONS /resource/id => Options(http.ResponseWriter, id string)*/
